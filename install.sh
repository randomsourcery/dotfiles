#!/usr/bin/env bash

# prerequisite: bash + ssh + curl

has() {
	command -v $1
}

setup_brew() {
	echo "setting up homebrew"
	if has brew ; then
		echo "already has brew, skipping setup"
		return 0
	fi

	echo "installing brew"
	bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)" || return 1

	echo "configuring brew path"
	ok=false
	prefixes=( "/opt/homebrew" "/usr/local" "/home/linuxbrew/.linuxbrew" )
	for prefix in "${prefixes[@]}"; do
		if [[ -f "$prefix/bin/brew" ]]; then
			echo "found in $prefix/bin/brew, configuring"
			eval "$($prefix/bin/brew shellenv)"
			ok=true
		fi
	done
	if [[ "$ok" == false ]]; then
		echo "no brew path found"
		return 1
	fi
}

install_pkg() {
	pkg="$1"
	echo "installing package $pkg"
	brew install "$pkg"
}

install_cmd() {
	cmd="$1"
	pkg="$2"
	if [[ -z "$pkg" ]]; then
		pkg="$cmd"
	fi
	echo "installing command $cmd from package $pkg"
	if has "$cmd" ; then
		echo "already has $cmd, skipping installation"
	else
		install_pkg "$pkg" || return 1
		has "$cmd"
	fi
}

setup_git() {
	echo "installing git"
	install_cmd git || exit 1
	echo "generating gitlab key"
	if [[ -f $HOME/.ssh/id_ed25519 ]]; then
		echo "already has gitlab key, skipping generation"
	else
		ssh-keygen -t ed25519 -C "minhnpq16@gmail.com" || return 1
		echo 'public key:'
		cat "$HOME/.ssh/id_ed25519.pub"
		echo 'ADD KEY to https://gitlab.com/-/profile/keys'
		read -s -k '?Press any key to continue.'
	fi
}

clone_dotfiles() {
	echo "cloning dotfiles"
	export DOTFILES="$HOME/dotfiles"
	mkdir -p $DOTFILES
	git clone git@gitlab.com:randomsourcery/dotfiles.git $DOTFILES || return 1
}

setup_zsh_plugins() {
	echo 'linking p10k config'
	ln -svi "$DOTFILES/.p10k.zsh" "$HOME"
	echo 'installing p10k'
	install_cmd p10k powerlevel10k || return 1

	echo 'installing zsh plugins'
	PLUGIN_DIR="$REPO/github.com/zsh-users"
	mkdir -p "$PLUGIN_DIR"
	pushd "$PLUGIN_DIR"
		git clone https://github.com/zsh-users/zsh-autosuggestions.git
		git clone https://github.com/zsh-users/zsh-history-substring-search.git
		git clone https://github.com/zsh-users/zsh-syntax-highlighting.git
	popd
}

setup_packages() {
	# mandatories
	set -ex # exit on fail

	echo "setting up code repo tree"
	export REPO="$HOME/repo"
	mkdir -p "$REPO"

	echo "setting up git"
	echo "linking git config"
	ln -svi "$DOTFILES/.gitconfig" "$HOME"
	ln -svi "$DOTFILES/.gitignore_global" "$HOME"

	echo "setting up zsh"
	echo "linking zsh config"
	ln -svi "$DOTFILES/.zshenv" "$HOME"
	ln -svi "$DOTFILES/.zshrc" "$HOME"
	install_cmd zsh
	setup_zsh_plugins

	brew tap homebrew/cask-fonts
	install_pkg font-caskaydia-mono-nerd-font

	install_cmd nvim neovim
	install_cmd fd
	install_cmd rg ripgrep
	install_cmd tldr
	install_cmd kitty
	install_cmd parallel

	echo "setting up fzf"
	install_pkg fzf
	$(brew --prefix)/opt/fzf/install

	echo "setting up go"
	echo "linking go path"
	mkdir -p "$HOME/go"
	ln -svi "$REPO" "$HOME/go/src"
	install_cmd go
	install_cmd gofumpt
	install_cmd gopls

	echo "setting up python"
	install_cmd python3 python
	install_cmd ruff
	install_cmd pyright

	echo "setting up lua"
	install_cmd lua-language-server

	install_pkg karabiner-elements

	brew install --cask nikitabobko/tap/aerospace

	# optionals
	set +ex
	install_pkg p7zip
	install_pkg wget
	install_pkg stats
	install_pkg firefox
	install_pkg spotify
	install_pkg sequel-ace
	install_pkg docker
	install_pkg kindle
	install_pkg postman
	install_pkg vlc
	install_pkg anki
	install_pkg postman
}

setup_brew || exit 1
setup_git || exit 1
clone_dotfiles || exit 1
setup_packages || exit 1
