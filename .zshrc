#!/usr/bin/env zsh

export REPO="$HOME/repo"
export DOTFILES="$HOME/dotfiles"

# ZSH vi mode
# need to set first because will mess up previously set keybinds
bindkey -v
export KEYTIMEOUT=1
autoload -Uz edit-command-line
zle -N edit-command-line
bindkey -M vicmd v edit-command-line


# Homebrew
prefixes=( "/opt/homebrew" "/usr/local" "/home/linuxbrew/.linuxbrew" )
for prefix in "${prefixes[@]}"; do
	[[ -f "$prefix/bin/brew" ]] && eval "$($prefix/bin/brew shellenv)"
done
export HOMEBREW_NO_AUTO_UPDATE=1


# Neovim
export EDITOR="nvim"
alias vim=nvim


# Git
alias g="git"
alias ga="git add"
alias gb="git branch"
alias gbl="git blame"
alias gc="git commit -v"
alias gda="git diff"
alias gd="gda --ignore-space-change"
alias gds="gd --staged"
alias gl="git log --graph --oneline --color"
alias gm="git merge"
alias gs="git status"
alias gsh="git show"
alias gps="git push"
alias gpl="git pull"
alias grb="git rebase"
alias grs="git reset"
alias gst="git stash -k -u"
alias gsp="git stash pop"
alias gcf="git config --global"
alias gsw="git switch"
alias grt="git restore"


# Ls
export LSCOLORS=ExGxBxDxCxEgEdxbxgxcxd
if ls --help 2>&1 | grep -q -- --color; then
	alias ls="ls --color=always"
	alias lp="ls --color=never"
else
	alias ls="ls -G"
fi
alias ll="ls -lh"
alias la="ll -a"
alias l.="ls -had .*"
alias ll.="ll -had .*"
alias lt="ll -t"


# Grep
alias grep="grep --color=always --exclude-dir=.git"
alias grepp="grep --color=never"


# Tldr
export TLDR_AUTO_UPDATE_DISABLED=true


# Go
export GOPATH="$HOME/go"
export PATH="$PATH:$GOPATH/bin"


# Ocaml
[[ ! -r '/Users/bytedance/.opam/opam-init/init.zsh' ]] || source '/Users/bytedance/.opam/opam-init/init.zsh' > /dev/null 2> /dev/null


# Fzf
export FZF_DEFAULT_COMMAND="fd --type file --hidden"
_fzf_compgen_path() {
  fd --hidden --follow . "$1"
}
_fzf_compgen_dir() {
  fd --type d --hidden . "$1"
}
source <(fzf --zsh)


# Zoxide
eval "$(zoxide init zsh)"
alias cd=z


# Direnv
eval "$(direnv hook zsh)"


# Zsh
HISTFILE="$HOME/.zsh_history"
HISTSIZE=10000000
SAVEHIST="$HISTSIZE"
setopt hist_ignore_all_dups
setopt hist_reduce_blanks
setopt inc_append_history # save history entries as soon as they are entered

bindkey '^N' history-substring-search-up
bindkey '^P' history-substring-search-down

# completion
autoload -U compinit
compinit
source "$REPO/github.com/zsh-users/zsh-autosuggestions/zsh-autosuggestions.zsh"
source "$REPO/github.com/zsh-users/zsh-history-substring-search/zsh-history-substring-search.zsh"

alias mkdir="mkdir -p -v"
alias ping="ping -c 5"

export LANG="en_US.UTF-8"
export LC_ALL="en_US.UTF-8"

# powerlevel10k
source "$(brew --prefix)/share/powerlevel10k/powerlevel10k.zsh-theme"
[[ ! -f ~/.p10k.zsh ]] || . ~/.p10k.zsh

# syntax highlighting
# keep at the END of file
typeset -A ZSH_HIGHLIGHT_STYLES
ZSH_HIGHLIGHT_STYLES[path]='fg=magenta'
source "$REPO/github.com/zsh-users/zsh-syntax-highlighting/zsh-syntax-highlighting.zsh"

# Host specific configurations
[[ -f "$HOME/local.rc.sh" ]] && source "$HOME/local.rc.sh"
export PATH="/opt/homebrew/opt/go@1.22/bin:$PATH"
export PATH="/opt/homebrew/opt/ruby/bin:$PATH"
