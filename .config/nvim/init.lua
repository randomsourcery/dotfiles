local map = function(modes, key, action, desc)
  vim.keymap.set(modes, key, action, { desc = desc })
end

local autocmd = vim.api.nvim_create_autocmd

-- remap leader first so that plugin-created keymaps map to correct leader
map({ "n", "v" }, "<Space>", "<Nop>")
vim.g.mapleader = " "

require("plugins")

vim.opt.termguicolors = true
vim.opt.lazyredraw = true
vim.opt.maxmempattern = 100000
vim.opt.undofile = true
vim.opt.swapfile = false
vim.opt.autoread = true
vim.opt.clipboard = "unnamedplus"
vim.opt.splitbelow = true
vim.opt.splitright = true
vim.opt.jumpoptions = "stack"

local fzf = require("fzf-lua")
map("n", "<leader><leader>", fzf.resume, "Resume Last Search")
map("n", "<leader>h", fzf.helptags, "Helps")
map("n", "<leader>f", fzf.files, "Files")
map("n", "<leader>b", fzf.buffers, "Buffers")
map("n", "<leader>u", vim.cmd.UndotreeToggle, "Undo Tree")

map("n", "<leader>dd", function()
  fzf.files({
    fd_opts = [[--color=never --type d --hidden --follow --exclude .git]],
  })
end, "Directories")
map("n", "<leader>dl", "<cmd>e %/..<cr>", "Directory of Local file")
map("n", "<leader>dt", "<cmd>lua MiniFiles.open()<cr>", "Directory Tree")

map("n", "<leader>t", "<cmd>enew<cr>", "Temporary buffer")

map("n", "<leader>gs", fzf.git_status, "Git Status")
map("n", "<leader>ga", "<cmd>!git add %<cr><cr>", "Git Add Current File")
map("n", "<leader>gc", fzf.git_commits, "Git Commits")
map("n", "<leader>gb", "<cmd>BlameToggle<cr>", "Git Blame")
map("n", "<leader>gm", function()
  local view = require("diffview.lib").get_current_view()
  if view then
    vim.cmd.DiffviewClose()
  else
    vim.cmd.DiffviewOpen()
  end
end, "Git Merge Toggle")


-- TEXT

vim.opt.scrolloff = 999 -- keep cursor at the middle of the screen
vim.opt.ignorecase = true
vim.opt.smartcase = true
vim.opt.virtualedit = "block"
vim.opt.number = true
vim.opt.relativenumber = true
vim.opt.colorcolumn = "120"

-- show cursorline on focused buffer only
autocmd({ "VimEnter", "WinEnter", "BufWinEnter" }, {
  pattern = "*",
  callback = function()
    vim.opt.cursorline = true
  end,
})
autocmd("WinLeave", {
  pattern = "*",
  callback = function()
    vim.opt.cursorline = false
  end,
})

vim.opt.expandtab = false
vim.opt.shiftwidth = 8
vim.opt.tabstop = 8
vim.opt.softtabstop = 8
vim.opt.smartindent = true
vim.opt.list = true
vim.opt.listchars = { leadmultispace = "▏   ", tab = "▏ ", trail = "~" }
vim.opt.linebreak = true
vim.opt.breakindent = true

local function update_indent_guide_width()
  local lead = "▏" .. string.rep(" ", vim.bo.shiftwidth - 1)
  vim.opt_local.listchars:append({ leadmultispace = lead })
end

autocmd("VimEnter", {
  callback = update_indent_guide_width,
  once = true,
})
autocmd("OptionSet", {
  pattern = { "listchars", "shiftwidth", "filetype" },
  callback = update_indent_guide_width,
})

map("n", "&", "0")
map("v", ">", ">gv", "Increase Indentation")
map("v", "<", "<gv", "Decrease Indentation")
map("n", "+", "<C-a>", "Increment Number")
map("n", "-", "<C-x>", "Decrement Number")
map("v", "y", "ygv<esc>", "Yank without jumping back to the start of the selection")

local subs = require("substitute")
map("n", "s", subs.operator)
map("n", "ss", subs.line)
map("n", "S", subs.eol)
map("v", "s", subs.visual)
local exchange = require("substitute.exchange")
map("n", "sx", exchange.operator)
map("n", "sxx", exchange.line)
map("x", "X", exchange.visual)
map("n", "sxc", exchange.cancel)

map("n", "<leader>?", "<cmd>FzfLua live_grep_native<cr>", "Text in Project")
map("n", "<leader>/", "<cmd>FzfLua grep_curbuf<cr>", "Text in Buffer")

local objs = require("various-textobjs")
map({ "x", "o" }, "gG", objs.entireBuffer)
map({ "x", "o" }, "as", function() objs.subword("outer") end)
map({ "x", "o" }, "is", function() objs.subword("inner") end)
map({ "x", "o" }, "aq", function() objs.anyQuote("outer") end)
map({ "x", "o" }, "iq", function() objs.anyQuote("inner") end)
map({ "x", "o" }, "Q", objs.toNextQuotationMark)
map({ "x", "o" }, "ab", function() objs.anyBracket("outer") end)
map({ "x", "o" }, "ib", function() objs.anyBracket("inner") end)
map({ "x", "o" }, "B", objs.toNextClosingBracket)
map({ "x", "o" }, "ai", function() objs.indentation("outer", "outer") end)
map({ "x", "o" }, "ii", function() objs.indentation("inner", "inner") end)


-- CODE

vim.diagnostic.config({
  underline = false,
})
vim.lsp.handlers["textDocument/hover"] = vim.lsp.with(vim.lsp.handlers.hover, {
  border = "rounded",
})

map("n", "<leader>cd", vim.lsp.buf.definition, "Code Definition")
map("n", "<leader>ct", vim.lsp.buf.type_definition, "Code Type Definition")
map("n", "<leader>cr", fzf.lsp_references, "Code References")
map("n", "<leader>ci", fzf.lsp_implementations, "Code Implementations")
map("n", "<leader>cp", fzf.diagnostics_workspace, "Code Problems/Diagnostics")
map("n", "<leader>cn", vim.lsp.buf.rename, "Code ReName")
map("n", "<leader>cf", vim.lsp.buf.format, "Code Format")
map({ "n", "v" }, "<leader>ca", vim.lsp.buf.code_action, "Code Actions")

autocmd("FileType", {
  pattern = "go",
  callback = function()
    -- https://go.dev/doc/effective_go#formatting
    vim.opt_local.expandtab = false
    vim.opt_local.shiftwidth = 8
    vim.opt_local.tabstop = 8
    vim.opt_local.softtabstop = 8
  end,
})

autocmd("FileType", {
  pattern = "python",
  callback = function()
    -- https://peps.python.org/pep-0008/#indentation
    vim.opt_local.expandtab = true
    vim.opt_local.shiftwidth = 4
    vim.opt_local.tabstop = 4
    vim.opt_local.softtabstop = 4
  end,
})

autocmd("FileType", {
  pattern = "lua",
  callback = function()
    -- https://lua-users.org/wiki/LuaStyleGuide
    vim.opt_local.expandtab = true
    vim.opt_local.shiftwidth = 2
    vim.opt_local.tabstop = 2
    vim.opt_local.softtabstop = 2
  end,
})

autocmd("FileType", {
  pattern = "rust",
  callback = function()
    -- https://doc.rust-lang.org/beta/style-guide/index.html
    vim.opt_local.expandtab = true
    vim.opt_local.shiftwidth = 4
    vim.opt_local.tabstop = 4
    vim.opt_local.softtabstop = 4
  end,
})
